from numba import jit, autojit
import numpy as np
import math
import time

@jit
def SOR_execute(omega, G, nn):
    o4 = omega*0.25
    o1 = 1.0 - omega
    nx = np.shape(G)[0]
    ny = np.shape(G)[1]
    for p in xrange(nn):
      for y in xrange(1, ny - 1):
        for x in xrange(1, nx - 1):
          G[x,y] = o4*(G[x,y-1]+G[x,y+1]+G[x-1,y]+G[x+1,y])+o1*G[x,y]

def SOR(cycles, args):
    n, = map(eval, args)
    a = np.random.rand(n,n)
    t0 = time.clock()
    SOR_execute(1.25, a, cycles)
    dt = time.clock() - t0
    return ((n-1)*(n-1)*cycles*6, dt)


@jit
def SparseCompRow_matmult(M, y, val, row, col, x, num_iterations):
    for reps in xrange(num_iterations):
        for r in xrange(M):
            sa = 0.0
            for i in xrange(row[r], row[r+1]):
                sa += x[ col[i] ] * val[i]
            y[r] = sa

def SMV(cycles, args):
    N, nz = map(int, args)
    x = np.zeros(N)
    y = np.zeros(N)
    nr = nz / N
    anz = nr * N
    val = np.zeros(anz)
    col = np.zeros([nz], dtype=np.int)
    row = np.zeros([N+1], dtype=np.int)
    row[0] = 0
    for r in xrange(N):
        rowr = row[r]
        step = r / nr
        row[r+1] = rowr + nr
        if (step < 1):
            step = 1
        for i in xrange(nr):
            col[rowr + i] = i * step
    nn=N
    cc=cycles
    t0 = time.clock()
    SparseCompRow_matmult(nn, y, val, row, col, x, cc);
    dt = time.clock() - t0
    return (anz*cycles*2, dt)

@jit
def MonteCarlo_integrate(Num_samples):
    under_curve = 0
    rr = np.random.rand(2*Num_samples)
    for count in xrange(0,Num_samples,2):
        x = rr[count]
        y = rr[count+1]
        if x*x + y*y <= 1.0:
            under_curve += 1
    return float(under_curve) / Num_samples * 4.0

def MC(cycles, args):
    t0 = time.clock()
    estimate_of_pi = MonteCarlo_integrate(cycles)
    dt = time.clock() - t0
    return (cycles*4, dt)

@jit
def LU_factor(A, pivot):
    M = np.shape(A)[0]
    N = np.shape(A)[1]
    minMN = min(M, N)
    tmp = np.zeros([N])
    for j in xrange(minMN):
        jp = j
        t = abs(A[j,j])
        for i in xrange(j + 1, M):
            ab = abs(A[i,j])
            if ab > t:
                jp = i
                t = ab
        pivot[j] = jp
        
        if A[jp,j] == 0:
            return 1

        if jp != j:
            tmp[:] = A[jp]
            A[jp,:] = A[j,:]
            A[j,:] = tmp

        if j < M-1:
            recp =  1.0 / A[j,j]
            for k in xrange(j + 1, M):
                A[k,j] *= recp

        if j < minMN-1:
            for ii in xrange(j + 1, M):
                for jj in xrange(j + 1, N):
                    A[ii,jj] -= A[ii,j] * A[j,jj]
    return 0

def LU(cycles, args):
    N, = map(int, args)
    A = np.random.rand(N,N)
    lu = np.random.rand(N,N)
    pivot = np.zeros([N], dtype=np.int32)
    t0 = time.clock()
    for i in xrange(cycles):
        lu[:,:] = A
        s = LU_factor(lu, pivot)
        if s == 1:
          raise Exception("factorization failed because of zero pivot")
    dt = time.clock() - t0
    return (2.0/3.0*N*N*N*cycles, dt)

@jit
def int_log2(n):
    k = 1
    log = 0
    while k < n:
        k *= 2
        log += 1
    if n != 1 << log:
        return 0 # Exception("FFT: Data length is not a power of 2: %s" % n)
    return log

@jit
def FFT_transform_internal(N, data, direction):
    n = N / 2
    bit = 0
    dual = 1
    if n == 1:
        return
    logn = int_log2(n)
    if N == 0:
        return
    FFT_bitreverse(N, data)

    # apply fft recursion
    # this loop executed int_log2(N) times
    bit = 0
    while bit < logn:
        w_real = 1.0
        w_imag = 0.0
        theta = 2.0 * direction * math.pi / (2.0 * float(dual))
        s = math.sin(theta)
        t = math.sin(theta / 2.0)
        s2 = 2.0 * t * t
        for b in range(0, n, 2 * dual):
            i = 2 * b
            j = 2 * (b + dual)
            wd_real = data[j]
            wd_imag = data[j + 1]
            data[j] = data[i] - wd_real
            data[j + 1] = data[i + 1] - wd_imag
            data[i] += wd_real
            data[i + 1] += wd_imag
        for a in xrange(1, dual):
            tmp_real = w_real - s * w_imag - s2 * w_real
            tmp_imag = w_imag + s * w_real - s2 * w_imag
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(0, n, 2 * dual):
                i = 2 * (b + a)
                j = 2 * (b + a + dual)
                z1_real = data[j]
                z1_imag = data[j + 1]
                wd_real = w_real * z1_real - w_imag * z1_imag
                wd_imag = w_real * z1_imag + w_imag * z1_real
                data[j] = data[i] - wd_real
                data[j + 1] = data[i + 1] - wd_imag
                data[i] += wd_real
                data[i + 1] += wd_imag
        bit += 1
        dual *= 2

@jit
def FFT_bitreverse(N, data):
    n = N / 2
    nm1 = n - 1
    j = 0
    for i in xrange(nm1):
        ii = i << 1
        jj = j << 1
        k = n >> 1
        if i < j:
            tmp_real = data[ii]
            tmp_imag = data[ii + 1]
            data[ii] = data[jj]
            data[ii + 1] = data[jj + 1]
            data[jj] = tmp_real
            data[jj + 1] = tmp_imag
        while k <= j:
            j -= k
            k >>= 1
        j += k

@jit
def FFT_transform(N, data):
    FFT_transform_internal(N, data, -1)

@jit
def FFT_inverse(N, data):
    n = N/2
    norm = 0.0
    FFT_transform_internal(N, data, +1)
    norm = 1 / float(n)
    for i in xrange(N):
        data[i] *= norm

def FFT(cycles, args):
    N, = map(int, args)
    twoN = 2*N
    l2n = math.log(N)/math.log(2)
    x = np.random.rand(twoN)
    t0 = time.clock()
    for i in xrange(cycles):
        FFT_transform(twoN, x)
        FFT_inverse(twoN, x)
    dt = time.clock() - t0
    return (((5*N-2)*l2n + 2*(N+1)) * cycles, dt)

def measure(mtm, funcname, args):
    func = eval(funcname)
    cycles = 1
    (flops, dt) = func(cycles, args) # Warm up JIT machinery.
    while 1:
        (flops, dt) = func(cycles, args)
        if dt >= mtm:
          print funcname, flops/dt*1.0e-6, cycles, dt
          #print funcname, flops/dt*1.0e-6
          return
        cycles = cycles * 2

def main():
    min_time = 2.0
    measure(min_time, 'FFT', ['1024'])
    measure(min_time, 'SOR', ['100'])
    measure(min_time, 'MC', [])
    measure(min_time, 'SMV', ['1000','5000'])
    measure(min_time, 'LU', ['100'])

if __name__ == '__main__': main()

# Large option
#measure(min_time, 'FFT', ['1048576'])
#measure(min_time, 'SOR', ['1000'])
#measure(min_time, 'MC', [])
#measure(min_time, 'SMV', ['100000','1000000'])
#measure(min_time, 'LU', ['1000'])
