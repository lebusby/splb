import math
import time
import numpy as np
from scipy.sparse import csr_matrix
import scipy
import scipy.linalg
import sys

def minmax(p, a):
  min = a.min()
  max = a.max()
  i1,j1 = np.unravel_index(a.argmin(), a.shape)
  i2,j2 = np.unravel_index(a.argmax(), a.shape)
  print >> sys.stderr, "%d: min=%.2e(%d,%d), max=%.2e(%d,%d)" % \
           (p, min, i1,j1, max, i2,j2)

def SOR_execute(omega, G, num_iterations):
    o4 = omega*0.25
    o1 = 1.0 - omega
    for p in xrange(num_iterations):
        G[1:-1,1:-1] = (o4* ( G[1:-1,0:-2] + G[1:-1,2:  ] + \
	                     G[0:-2,1:-1] + G[2:  ,1:-1] ) + \
			     o1*G[1:-1,1:-1])

def SOR(cycles, args):
    n, = map(int, args)
    np.random.seed(7)
    a = np.random.rand(n,n)
    t0 = time.clock()
    # We actually use Jacobi iteration in SOR_execute, so must set omega<=1.
    SOR_execute(1.0, a, cycles)
    dt = time.clock() - t0
    return ((n-1)*(n-1)*cycles*6, dt)

def SparseMult(A, y, x, num_iterations):
    for reps in xrange(num_iterations):
        y = A.dot(x)

def SMV(cycles, args):
    N, nz = map(int, args)
    x = np.random.random(N)
    y = np.zeros(N)
    nr = nz / N
    anz = nr * N
    val = np.random.random(anz)
    col = np.zeros(nz, 'int')
    row = np.zeros(N+1, 'int')
    row[0] = 0
    for r in xrange(N):
        rowr = row[r]
        step = r / nr
        row[r+1] = rowr + nr
        if (step < 1):
            step = 1
        for i in xrange(nr):
            col[rowr + i] = i * step
    A = csr_matrix((val,col,row),shape=(N,N))
    t0 = time.clock()
    SparseMult(A, y, x, cycles);
    dt = time.clock() - t0
    return (anz*cycles*2, dt)

def MonteCarlo_integrate(Num_samples):
    np.random.seed(113)
    x = np.random.random(Num_samples)
    y = np.random.random(Num_samples)
    inside = np.sqrt(x**2+y**2) <= 1
    return sum(inside).astype('double') / Num_samples * 4.0

def MC(cycles, args):
    t0 = time.clock()
    estimate_of_pi = MonteCarlo_integrate(cycles)
    dt = time.clock() - t0
    return (cycles*4, dt)

def LU_factor(A):
    P,L,U = scipy.linalg.lu(A)

def LU(cycles, args):
    N, = map(int, args)
    np.random.seed(7)
    A = np.random.rand(N,N)
    lu = np.zeros_like(A)
    t0 = time.clock()
    for i in xrange(cycles):
        #np.copyto(lu,A)
        lu[:,:] = A
        LU_factor(lu)
    dt = time.clock() - t0
    return (2.0/3.0*N*N*N*cycles, dt)

def FFT(cycles, args):
    N, = map(int, args)
    twoN = 2*N
    l2n = math.log(N)/math.log(2)
    np.random.seed(7)
    x = np.random.random(twoN)
    t0 = time.clock()
    for i in xrange(cycles):
        x = np.fft.fft(x)
        x = np.fft.ifft(x)
    dt = time.clock() - t0
    return (((5*N-2)*l2n + 2*(N+1)) * cycles, dt)

def measure(mtm, funcname, args):
    func = eval(funcname)
    cycles = 1
    while 1:
        (flops, dt) = func(cycles, args)
        if dt >= mtm:
          #print funcname, flops/dt*1.0e-6, cycles, dt
          print funcname, flops/dt*1.0e-6
          return
        cycles = cycles * 2

def main():
    min_time = 2.0
    measure(min_time, 'FFT', ['1024'])
    measure(min_time, 'SOR', ['100'])
    measure(min_time, 'MC', [])
    measure(min_time, 'SMV', ['1000','5000'])
    measure(min_time, 'LU', ['100'])

if __name__ == '__main__': main()

# Large option
#measure(min_time, 'FFT', ['1048576'])
#measure(min_time, 'SOR', ['1000'])
#measure(min_time, 'MC', [])
#measure(min_time, 'SMV', ['100000','1000000'])
#measure(min_time, 'LU', ['1000'])
