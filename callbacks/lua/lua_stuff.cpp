// Copyright (c) 2014, Lawrence Livermore National Security, LLC. Produced
// at the Lawrence Livermore National Laboratory.  Written by Lee Busby,
// busby1@llnl.gov. LLNL-CODE-660478. All rights reserved.
// See ../../Copyright for additional notices.

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <lua.hpp>
#include "lua_stuff.hpp"

// Implementation of TinyLuaFn.
TinyLuaFn::~TinyLuaFn() { }

double TinyLuaFn::Eval(const double *v)
{
   lua_rawgeti(L, LUA_REGISTRYINDEX, fref);
   if (lua_isfunction(L, -1)) // May be number or function.
   {
      for (int i=0; i<narg; ++i)
      {
         lua_pushnumber(L, v[i]);
      }
      if (lua_pcall(L, narg, 1, 0) != 0)
      {
         lutil::error(L, "error calling function: %s", lua_tostring(L, -1));
      }
   }
   double rv = lua_tonumber(L, -1);
   lua_pop(L, 1); // Maintain stack.
   return rv;
}

// Utility functions for various lua-related duties.
namespace lutil
{

   void error(lua_State *L, const char *fmt, ...)
   {
      va_list argp;
      va_start(argp, fmt);
      vfprintf(stderr, fmt, argp);
      va_end(argp);
      lua_close(L);
      exit(EXIT_FAILURE);
   }

   void itemdump(lua_State *L, int i, char c)   // debugging tool.
   {
      int t = lua_type(L, i);
      switch (t)
      {
      case LUA_TSTRING:
      {
         fprintf(stderr, "%d: (string): %s%c", i, lua_tostring(L,i), c);
         break;
      }
      case LUA_TBOOLEAN:
      {
         fprintf(stderr, "%d: (bool): %s%c",
                 i, lua_toboolean(L, i) ? "true":"false", c);
         break;
      }
      case LUA_TNUMBER:
      {
         fprintf(stderr, "%d: (number): %g%c", i, lua_tonumber(L, i), c);
         break;
      }
      default:
      {
         fprintf(stderr, "%d: (%s): %x%c",
                 i, lua_typename(L, t), lua_topointer(L,i), c);
         break;
      }
      }
   }

   void stackdump(lua_State *L)   // debugging tool.
   {
      int i, top = lua_gettop(L);
      for (i=1; i<= top; i++)
      {
         itemdump(L, i, '|');
      }
      fprintf(stderr, "\n");
   }

   TinyLuaFn *new_lua_fn(lua_State *L, int nargs)
   {
      int ref = luaL_ref(L, LUA_REGISTRYINDEX);
      lua_pushnil(L); // stack size to remain unchanged.
      return new TinyLuaFn(L, ref, nargs);
   }
  
}
