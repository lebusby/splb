// Copyright (c) 2014, Lawrence Livermore National Security, LLC. Produced
// at the Lawrence Livermore National Laboratory.  Written by Lee Busby,
// busby1@llnl.gov. LLNL-CODE-660478. All rights reserved.
// See ../../Copyright for additional notices.

#ifndef LUA_STUFF
#define LUA_STUFF

#include <string>
#include <vector>
#include "lua.hpp"

// pushglobaltable is from Lua 5.2.  GLOBALSINDEX is 5.1.
#ifdef LUA_GLOBALSINDEX
#define lua_pushglobaltable(L) lua_pushvalue(L, LUA_GLOBALSINDEX)
#define lua_rawlen(L,i)        lua_objlen(L,i)
#endif

class TinyLuaFn
{
public:
   TinyLuaFn(lua_State *L, int r, int n) : L(L), fref(r), narg(n) { };
   ~TinyLuaFn();
   double Eval(const double *v);
private:
   lua_State *L;
   int fref;
   int narg;
};

namespace lutil
{
   void error (lua_State *L, const char *fmt, ...);
   void itemdump(lua_State *L, int i, char c);
   void stackdump(lua_State *L);
   TinyLuaFn *new_lua_fn(lua_State *L, int nargs);
}

#endif
