// Copyright (c) 2014, Lawrence Livermore National Security, LLC. Produced
// at the Lawrence Livermore National Laboratory.  Written by Lee Busby,
// busby1@llnl.gov. LLNL-CODE-660478. All rights reserved.
// See ../Copyright for additional notices.

#include "fp/fparser.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <sys/time.h>
#include <cmath>
#include <stdlib.h>

#define NFN 40

clock_t myclock(void) {
  struct timespec ts;
  (void) clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
  return (clock_t)(1000000*ts.tv_sec + ts.tv_nsec/1000);
}

int main(int argc, char *argv[]) {
    int i, j, f, loops=1000;
    clock_t start, end, split[NFN] = {0};
    double psum[NFN] = {0.}, v1[1000], v2[1000];

    int lcnt;
    double v[2];
    FunctionParser fp[NFN];
    std::ifstream input("e1.fp");
    std::string line;

    if (argc > 1) loops = atoi(argv[1]);

    for (lcnt=0; std::getline(input,line); lcnt++) {
      fp[lcnt].AddConstant("pi", M_PI);
      fp[lcnt].AddConstant("e", M_E);
      int res = fp[lcnt].Parse(line, "a,b");
      if(res >= 0) {
          std::cerr << "Error in function: " << line << '\n';
          return res;
      }
      fp[lcnt].Optimize();
    }
    input.close();

    srand48(3);
    for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
      v1[i] = drand48();
      v2[i] = drand48();
    }

    for (f=0;f<lcnt;f++) {
      start = myclock();
      for (j=0;j<loops;j++) {
        for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
          v[0] = v1[i];
          v[1] = v2[i];
          psum[f] += fp[f].Eval(v);
        }
      }
      end = myclock();
      split[f] = end - start;
    }

    for (f=0;f<lcnt;f++)
      printf("psum[%02d]: %20.12e t: %12d\n", f, psum[f], split[f]);

    return 0;
}
