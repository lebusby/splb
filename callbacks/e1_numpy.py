from numpy import *

def f00(a,b):
  return sin((1.0+2.0/2.0*3.0)*4.0**5)+cos(6.0*pi)
def f01(a,b):
  return a+1.0
def f02(a,b):
  return a*2.0
def f03(a,b):
  return 2.0*a+1.0
def f04(a,b):
  return (2.0*a+1.0)*3.0
def f05(a,b):
  return 1.1*a**2 + 2.2*b**3
def f06(a,b):
  return 1.1*a**2.01 + 2.2*b**3.01
def f07(a,b):
  return 1.0/(a*sqrt(2.0*pi))*e**(-0.5*((b-a)/a)**2)
def f08(a,b):
  return (((((((7.0*a+6.0)*a+5.0)*a+4.0)*a+3.0)*a+2.0)*a+1.0)*a+0.1)
def f09(a,b):
  return 7.0*a**7 + 6.0*a**6 + 5.0*a**5 + 4.0*a**4 + 3.0*a**3 + 2.0*a**2 + 1.0*a**1 + 0.1
def f10(a,b):
  return sqrt(a**2+b**2)
def f11(a,b):
  return sin(a)
def f12(a,b):
  return sqrt(abs(a))
def f13(a,b):
  return abs(a)
def f14(a,b):
  return (a/((((b+(((e*(((((pi*((((3.45*((pi+a)+pi))+b)+b)*a))+0.68)+e)+a)/a))+a)+b))+b)*a)-pi))
def f15(a,b):
  return a+(cos(b-sin(2/a*pi))-sin(a-cos(2*b/pi)))-b
def f16(a,b):
  return sin(a)+sin(b)
def f17(a,b):
  return abs(sin(sqrt(a**2+b**2))*255.0)
def f18(a,b):
  return (b+a/b) * (a-b/a)
def f19(a,b):
  return (0.1*a+1.0)*a+1.1-sin(a)-log(a)/a*3.0/4.0
def f20(a,b):
  return sin(2.0 * a) + cos(pi / b)
def f21(a,b):
  return 1.0 - sin(2.0 * a) + cos(pi / b)
def f22(a,b):
  return sqrt(abs(1.0 - sin(2.0 * a) + cos(pi / b) / 3.0))
def f23(a,b):
  return 1.0-(a/b*0.5)
def f24(a,b):
  return 10.0**log(3.0+b)
def f25(a,b):
  return (cos(2.41)/b)
