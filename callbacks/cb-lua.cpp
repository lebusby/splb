// Copyright (c) 2014, Lawrence Livermore National Security, LLC. Produced
// at the Lawrence Livermore National Laboratory.  Written by Lee Busby,
// busby1@llnl.gov. LLNL-CODE-660478. All rights reserved.
// See ../Copyright for additional notices.

#include "lua_stuff.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <sys/time.h>
#include <cmath>
#include <stdlib.h>
#include <stdio.h>

#define NFN 40

clock_t myclock(void) {
  struct timespec ts;
  (void) clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
  return (clock_t)(1000000*ts.tv_sec + ts.tv_nsec/1000);
}

int main(int argc, char *argv[]) {
    int i, j, f, loops=1000;
    clock_t start, end, split[NFN] = {0};
    double psum[NFN] = {0.}, v1[1000], v2[1000];

    char fname[4];
    int lcnt;
    std::string input("e1.lua");
    double v[2];
    TinyLuaFn *fp[NFN];
    lua_State *L = luaL_newstate();

    if (argc > 1) loops = atoi(argv[1]);

    luaL_openlibs(L);
    if (luaL_loadfile(L, input.c_str()) || lua_pcall(L, 0, 0, 0))
    {
       std::cerr << "cannot read input file: " << lua_tostring(L, -1) << "\n";
       return 1;
    }

    for (f=0;f<NFN;f++) {
      (void)sprintf(fname, "f%02d", f);
      lua_getglobal(L, fname);
      if (lua_isnil(L, -1)) {
        break;
      }
      fp[f] = lutil::new_lua_fn(L, 2); // a,b
    }
    lcnt = f;

    srand48(3);
    for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
      v1[i] = drand48();
      v2[i] = drand48();
    }

    for (f=0;f<lcnt;f++) {
      start = myclock();
      for (j=0;j<loops;j++) {
        for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
          v[0] = v1[i];
          v[1] = v2[i];
          psum[f] += fp[f]->Eval(v);
        }
      }
      end = myclock();
      split[f] = end - start;
    }

    for (f=0;f<lcnt;f++)
      printf("psum[%02d]: %20.12e t: %12d\n", f, psum[f], split[f]);

    return 0;
}
