// Copyright (c) 2014, Lawrence Livermore National Security, LLC. Produced
// at the Lawrence Livermore National Laboratory.  Written by Lee Busby,
// busby1@llnl.gov. LLNL-CODE-660478. All rights reserved.
// See ../Copyright for additional notices.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <sys/time.h>
#include <Python.h>

clock_t myclock(void) {
  struct timespec ts;
  (void) clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
  return (clock_t)(1000000*ts.tv_sec + ts.tv_nsec/1000);
}

#define NFN 40

int main(int argc, char *argv[]) {
    int i, j, f, loops=1000;
    clock_t start, end, split[NFN] = {0};
    double psum[NFN] = {0.}, v1[1000], v2[1000];

    char fname[4];
    int lcnt;
    PyObject *pName, *pModule, *pFunc, *pArgs, *pValue;
    PyObject *fp[NFN];

    if (argc > 1) loops = atoi(argv[1]);

    srand48(3);
    for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
      v1[i] = drand48();
      v2[i] = drand48();
    }

    setenv("PYTHONPATH",".",1);
    Py_Initialize();
    pName = PyString_FromString("e1");
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    for (f=0;f<NFN;f++) {
      (void)sprintf(fname, "f%02d", f);
      pFunc = PyObject_GetAttrString(pModule, fname);
      if (pFunc == NULL) {
        break;
      }
      Py_INCREF(pFunc);
      fp[f] = pFunc;
    }
    lcnt = f;
    Py_DECREF(pModule);

    for (f=0;f<lcnt;f++) {
      start = myclock();
      for (j=0;j<loops;j++) {
        for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
          pArgs = PyTuple_New(2);
          pValue = PyFloat_FromDouble(v1[i]);
          PyTuple_SetItem(pArgs, 0, pValue);
          pValue = PyFloat_FromDouble(v2[i]);
          PyTuple_SetItem(pArgs, 1, pValue);
          pValue = PyObject_CallObject(fp[f], pArgs);
          psum[f] += PyFloat_AsDouble(pValue);
          Py_DECREF(pValue);
          Py_DECREF(pArgs);
        }
      }
      end = myclock();
      split[f] = end - start;
    }

    for (f=0;f<lcnt;f++)
      printf("psum[%02d]: %20.12e t: %12d\n", f, psum[f], split[f]);

    return 0;
}
