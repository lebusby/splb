// Copyright (c) 2014, Lawrence Livermore National Security, LLC. Produced
// at the Lawrence Livermore National Laboratory.  Written by Lee Busby,
// busby1@llnl.gov. LLNL-CODE-660478. All rights reserved.
// See ../Copyright for additional notices.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <sys/time.h>

double f00(double a, double b) { return sin((1.0+2.0/2.0*3.0)*std::pow(4,5))+cos(6.0*M_PI); }
double f01(double a, double b) { return a+1.0; }
double f02(double a, double b) { return a*2.0; }
double f03(double a, double b) { return 2.0*a+1.0; }
double f04(double a, double b) { return (2.0*a+1.0)*3.0; }
double f05(double a, double b) { return 1.1*std::pow(a,2) + 2.2*std::pow(b,3); }
double f06(double a, double b) { return 1.1*std::pow(a,2.01) + 2.2*std::pow(b,3.01); }
double f07(double a, double b) { return 1.0/(a*std::sqrt(2*M_PI))*std::pow(M_E,(-0.5*std::pow(((b-a)/a),2))); }
double f08(double a, double b) { return (((((((7.0*a+6.0)*a+5.0)*a+4.0)*a+3.0)*a+2.0)*a+1.0)*a+0.1); }
double f09(double a, double b) { return 7.0*std::pow(a,7) + 6.0*std::pow(a,6) + 5.0*std::pow(a,5) + 4.0*std::pow(a,4) + 3.0*std::pow(a,3) + 2.0*std::pow(a,2) + 1.0*std::pow(a,1) + 0.1; }
double f10(double a, double b) { return sqrt(std::pow(a,2)+std::pow(b,2)); }
double f11(double a, double b) { return sin(a); }
double f12(double a, double b) { return sqrt(fabs(a)); }
double f13(double a, double b) { return fabs(a); }
double f14(double a, double b) { return (a/((((b+(((M_E*(((((M_PI*((((3.45*((M_PI+a)+M_PI))+b)+b)*a))+0.68)+M_E)+a)/a))+a)+b))+b)*a)-M_PI)); }
double f15(double a, double b) { return a+(cos(b-sin(2/a*M_PI))-sin(a-cos(2*b/M_PI)))-b; }
double f16(double a, double b) { return sin(a)+sin(b); }
double f17(double a, double b) { return fabs(sin(sqrt(std::pow(a,2)+std::pow(b,2)))*255.0); }
double f18(double a, double b) { return (b+a/b) * (a-b/a); }
double f19(double a, double b) { return (0.1*a+1.0)*a+1.1-sin(a)-log(a)/a*3.0/4.0; }
double f20(double a, double b) { return sin(2.0 * a) + cos(M_PI / b); }
double f21(double a, double b) { return 1.0 - sin(2.0 * a) + cos(M_PI / b); }
double f22(double a, double b) { return sqrt(fabs(1.0 - sin(2.0 * a) + cos(M_PI / b) / 3.0)); }
double f23(double a, double b) { return 1.0-(a/b*0.5); }
double f24(double a, double b) { return std::pow(10,log(3.0+b)); }
double f25(double a, double b) { return (cos(2.41)/b); }

struct func { double (*fn)(double a, double b); } flist[] = {
  f00, f01, f02, f03, f04, f05, f06, f07, f08, f09,
  f10, f11, f12, f13, f14, f15, f16, f17, f18, f19,
  f20, f21, f22, f23, f24, f25,
};

clock_t myclock(void) {
  struct timespec ts;
  (void) clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
  return (clock_t)(1000000*ts.tv_sec + ts.tv_nsec/1000);
}

//clock_t myclock(void) {
//#define July1 1404198000
//  struct timeval tv;
//  (void) gettimeofday(&tv, 0);
//  return (clock_t)(tv.tv_usec + 1000000*(tv.tv_sec-July1));
//}

#define NFN sizeof(flist)/sizeof(flist[0])

int main(int argc, char *argv[]) {
    int i, j, f, loops=1000;
    clock_t start, end, split[NFN] = {0};
    double psum[NFN] = {0.}, v1[1000], v2[1000];

    if (argc > 1) loops = atoi(argv[1]);

    srand48(3);
    for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
      v1[i] = drand48();
      v2[i] = drand48();
    }

    for (f=0;f<NFN;f++) {
      start = myclock();
      for (j=0;j<loops;j++) {
        for (i=0;i<sizeof(v1)/sizeof(v1[0]);i++){
          psum[f] += flist[f].fn(v1[i], v2[i]);
        }
      }
      end = myclock();
      split[f] = end - start;
    }

    for (f=0;f<NFN;f++)
      printf("psum[%02d]: %20.12e t: %12d\n", f, psum[f], split[f]);

    return 0;
}
